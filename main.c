#include<stdio.h>
#include "addition.h"
#include "multiplication.h"


int main(int argc, char *argv[]){

float n1, n2;

// read number choices from user
printf("Enter a number: ");
scanf("%f", &n1);
printf("Enter another number: ");
scanf("%f", &n2);

// print numbers picked by user
printf("n1 = %.2f\n", n1);
printf("n2 = %.2f\n", n2);

// output results
printf("n1+n2 = %.4f \n", addition(n1,n2));
printf("n1*n2 = %.4f \n", multiplication(n1,n2));

return 0;

/*
-xc -std=c99 --target=arm-arm-none-eabi -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -c \
-fno-rtti -funsigned-char -fshort-enums -fshort-wchar \
-D__RTX -gdwarf-4 -O1 -ffunction-sections -Wno-packed -Wno-missing-variable-declarations \
-Wno-missing-prototypes -Wno-missing-noreturn -Wno-sign-conversion -Wno-nonportable-include-path \
-Wno-reserved-id-macro -Wno-unused-macros -Wno-documentation-unknown-command -Wno-documentation \
-Wno-license-management -Wno-parentheses-equality
*/

}