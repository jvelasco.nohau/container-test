# ---------------------------------------------------
# Simple make file to test different compilers
# under default conditions within a Docker Container
# Nohau Solutions AB
# --------------------------------------------------

#CC = gcc
CC = armclang --target=arm-arm-none-eabi -mcpu=cortex-m4

# name of my output binary
TARGET= calculator

# Collect names of all .c extension files
SRC=$(wildcard *.c)

# Create names for object files (.o) based on source (.c) files
# by substituting the .c extension for .o
OBJS=$(SRC:%.c=%.o)

all: $(TARGET)

# ---------------- Expanded version -----------------
# $(CC) to be able to switch compiler models.
#calculator: main.o addition.o multiplication.o
#	$(CC) -o calculator main.o addition.o multiplication.o

#main.o: main.c 
#	$(CC) -c -o main.o main.c 

#addition.o: addition.c 
#	$(CC) -c -o addition.o addition.c

#multiplication.o: multiplication.c 
#	$(CC) -c -o multiplication.o multiplication.c
# ---------------- Expanded version -----------------

# ---------------- Parametrized version -------------

# Build output binary by linking object files
$(TARGET): $(OBJS)
	$(CC) -o $@ $^

# create object files for every .c file
%.o: %.c 
	$(CC) -c -o $@ $<
# ---------------- Parametrized version -------------

# remove object files and output binary
# del .\calculator.* .\*.o
.PHONY:clean
clean:
	rm $(TARGET) *.o
